
布咕是基于CIM组件开发的一整套完整的产品,面向所有人开放注册的试用场景。具有丰富的功能，音视频会议，聊天、群组、好友，组织架构、公众号、朋友圈等功能。不依赖任何第三方云服务，可以私有化部署。

### 声明

**1、已经永久关闭开源代码(体验版)公开下载。**
 
**2、获取源码授权需要收费并实名制，验证个人或者企业身份。** 

**3、任何个人或者组织不得使用本项目做违法业务。**



---
## 官网 [http://farsunset.com](http://farsunset.com) 
--- 

## 音视频通话
可以支持单人的音视频通话，清晰流畅
支持多人音视频会议
体验与交互符合用户常用习惯
<div align="center">
   <img src="http://staticres.oss-cn-hangzhou.aliyuncs.com/hoxin/call_video_incoming.jpg" width="45%"  />
   <img src="http://staticres.oss-cn-hangzhou.aliyuncs.com/hoxin/group_video_calling.jpg" width="45%" />
</div>

## 暗黑模式
适配Android10 暗黑主题
匠心独运，全组件全方位的色彩适配
暗黑与光亮，你来选择
<div align="center">
   <img src="http://staticres.oss-cn-hangzhou.aliyuncs.com/hoxin/single_chatting_dark.jpg" width="45%"  />
   <img src="http://staticres.oss-cn-hangzhou.aliyuncs.com/hoxin/single_chatting_light.jpg" width="45%" />
</div>



## 朋友圈功能
流畅顺滑体验，堪比微信
可以分享图片和文字
还可以分享小视频，网页链接，以及点赞,评论互动
<div align="center">
   <img src="http://staticres.oss-cn-hangzhou.aliyuncs.com/hoxin/moment_timeline_dark.jpg" width="45%"  />
   <img src="http://staticres.oss-cn-hangzhou.aliyuncs.com/hoxin/moment_timeline_light.jpg" width="45%" />
</div>


## 公众号
功能原理完全和微信保持一致
独立微服务的接入，自定义菜单，自定义响应内容
<div align="center">
   <img src="http://staticres.oss-cn-hangzhou.aliyuncs.com/hoxin/ms_chat.jpg" width="45%"/>
   <img src="http://staticres.oss-cn-hangzhou.aliyuncs.com/hoxin/ms_chat_dark.jpg" width="45%"/>
</div>
